class ApplicationMailer < ActionMailer::Base
  default from: "registration@powderfriend.com"
  # default from: "from@example.com"
  # layout 'mailer'
end
