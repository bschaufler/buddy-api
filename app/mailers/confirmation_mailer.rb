class ConfirmationMailer < ApplicationMailer
  # Action Mailer
  def send_confirmation(user_email)
    # @order = order
    # @user = @order.user
    mail to: user_email, subject: "Registration"
  end
end
