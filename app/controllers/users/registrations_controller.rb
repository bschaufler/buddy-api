module Users
  class RegistrationsController < Devise::RegistrationsController


    def create
      build_resource permitted_params.except(:member_id)
      resource.save!
      # resource.send_account_created_email
      data = {
          token: resource.authentication_token,
          email: resource.email,
          stripe_id: resource.stripe_id,
          id: resource.id
      }
      ConfirmationMailer.send_confirmation(data[:email]).deliver_later
      render json: data, status: 201
    rescue => error
      binding.pry
      respond_with(
          resource,
          reason: error,
          location: new_user_registration_path,
          status:   :unprocessable_entity
      )
    end

    private

    def permitted_params
      params.permit(
          :answer_1,
          :answer_2,
          :email,
          :member_id,
          :password,
          :password_confirmation,
          :question_1,
          :question_2
      )
    end
  end
end
