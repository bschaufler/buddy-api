module Authenticable

  # Devise methods overwrites
  def current_user
    @current_user ||= User.find_by(authentication_token: clean_auth_header)
  end

  def clean_auth_header
    if Rails.env.test?
      request.headers['Authorization']
    else
      if (request.headers['Authorization'])
        request.headers['Authorization'].split(',').first.split('=')[1].tr!('"', '')
      end
    end
  end

  def authenticate_with_token
    render json: { errors: "Not authenticated" + request.headers['Authorization']}, status: :unauthorized unless user_signed_in?
  end

  def user_signed_in?
    current_user.present?
  end

end
