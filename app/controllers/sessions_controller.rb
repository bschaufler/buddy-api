class SessionsController < Devise::SessionsController
  def create
    respond_to do |format|
      format.json do
        self.resource = warden.authenticate!(auth_options)
        sign_in(resource_name, resource)
        plan_id = sync_with_stripe_subscriptions(resource)
        data = {
            token: self.resource.authentication_token,
            email: self.resource.email,
            stripe_id: self.resource.stripe_id,
            id: self.resource.id,
            planId: plan_id
        }
        render json: data, status: 201
      end
    end
  end

  def sync_with_stripe_subscriptions(user)
    plan_id = nil
    if (user.subscriptions.any?)
      first_subscription = user.subscriptions.first
      stripe_service = StripeService.new
      result = stripe_service.check_subscription(first_subscription.stripe_subscription_id)
      first_subscription.is_active = result[:is_active];
      first_subscription.save
      plan_id = result[:plan_id]
    end
    plan_id
  end
  # def create
  #   byebug
  #   user_password = params[:session][:password]
  #   user_email = params[:session][:email]
  #   user = user_email.present? && User.find_by(email: user_email)
  #
  #   if user.valid_password? user_password
  #     sign_in user, store: false
  #     user.generate_authentication_token!
  #     user.save
  #     render json: user, status: 200, location: [:api, user]
  #   else
  #     render json: { errors: "Invalid email or password" }, status: 422
  #   end
  # end
end
