class Api::V1::OneTimePurchaseController < ApplicationController
  before_action :authenticate_with_token
  respond_to :json

  # def show
  #   if !params[:id]
  #     render json: {}, status: 403
  #   else
  #     stripe_service = StripeService.new
  #     user = stripe_service.get_customer(current_user.stripe_id)
  #     if !(stripe_service.check_subscription(user, params[:id]))
  #       render json: {}, status: 404
  #
  #       subscription = stripe_service.get_subscription(params[:id])
  #       render json: subscription
  #     end
  #     end
  # end

  # def update
  #
  # end

  # def destroy
  #   params[:id]
  #   stripe_service = StripeService.new
  #   stripe_service.cancel_subscription(current_user.stripe_id)
  # end

  def create
    amount = params[:amount].to_i
    listing_id = params[:listing_id]
    stripe_service = StripeService.new
    if (params[:default_source])
      stripe_service.update_customer_default(current_user.stripe_id, params[:default_source])
    end
    stripe_service.create_charge(current_user.stripe_id, amount)
    current_user.purchased_listings.build({"listing_id" => listing_id, "user_id" => current_user.id})
    current_user.save
    render json:{}, status: 201
  end

  private

  def subscription_params
    params.permit(:id)
  end
end
