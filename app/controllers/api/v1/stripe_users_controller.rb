class Api::V1::StripeUsersController < ApplicationController
  before_action :authenticate_with_token
  respond_to :json

  def show
    if params[:id] != current_user.stripe_id
      render json: {}, status: 403
    else
      stripe_service = StripeService.new
      user = stripe_service.get_customer(current_user.stripe_id)
      render json: user
    end
  end

  def update
    if params[:id] != current_user.stripe_id
      render json: {}, status: 403
    else
      stripe_service = StripeService.new
      if (user_params[:name])
        user = stripe_service.update_card(params[:id], user_params[:source], user_params[:name])
      else
        user = stripe_service.update_customer(params[:id], user_params[:source])
      end

      if user
        render json: user.to_json, status: 200
      else
        render json: { errors: stripe.errors }, status: 422
      end
    end
  end

  def destroy
    if params[:id] != current_user.stripe_id
      render json: {}, status: 403
    else
      stripe_service = StripeService.new
      response = stripe_service.delete_card(params[:id], user_params[:source])
      if response
        head 204
      else
        head 500
      end
    end
  end

  private

  def user_params
    params.require(:user).permit(:source, :name)
  end
end
