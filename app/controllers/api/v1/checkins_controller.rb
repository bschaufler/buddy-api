class Api::V1::CheckinsController < ApplicationController
  respond_to :json
  before_action :authenticate_with_token, only: [:create]
  def show
    respond_with Checkin.find(params[:id])
  end

  def index
    checkins = current_user.checkins
    render json: checkins
  end

  def create
    checkin = current_user.checkins.build(checkin_params)
    if checkin.save
      render json: checkin, status: 201, location: [:api, checkin]
    else
      render json: { errors: checkin.errors }, status: 422
    end
  end

  def update
    listing = current_user.checkins.find(params[:id])
    if listing.update(listing_params)
      render json: checkin, status: 200, location: [:api, listing]
    else
      render json: { errors: checkin.errors }, status: 422
    end
  end

  def destroy
    current_user.checkins.delete_all
    head 204
  end

  private

  def checkin_params
    params.require(:data).require(:attributes).permit(:location)
  end
end
