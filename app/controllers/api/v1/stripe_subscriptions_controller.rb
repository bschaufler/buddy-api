class Api::V1::StripeSubscriptionsController < ApplicationController
  before_action :authenticate_with_token
  respond_to :json

  def show
    if !params[:id]
      render json: {}, status: 403
    else
      stripe_service = StripeService.new
      user = stripe_service.get_customer(current_user.stripe_id)
      if !(stripe_service.check_subscription(user, params[:id]))
        render json: {}, status: 404

        subscription = stripe_service.get_subscription(params[:id])
        render json: subscription
      end
      end
  end

  def update

  end

  def destroy
    params[:id]
    stripe_service = StripeService.new
    stripe_service.cancel_subscription(current_user.stripe_id)
  end

  def create
    if (current_user.subscriptions.length > 0)
      render json {"subscription already exist."}, status: 409
    end
    plan = params[:plan]
    stripe_service = StripeService.new
    stripe_service.update_customer_default(current_user.stripe_id, params[:default_source])
    response = stripe_service.create_subscription(current_user.stripe_id, plan)
    subscription = current_user.subscriptions.build({"stripe_subscription_id" => response["id"], "is_active" => true})
    current_user.save
    if subscription.save
      render json:{}, status: 201
    end
  end

  private

  def subscription_params
    params.permit(:id)
  end
end
