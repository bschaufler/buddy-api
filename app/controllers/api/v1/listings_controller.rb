class Api::V1::ListingsController < ApplicationController
  respond_to :json
  before_action :authenticate_with_token, only: [:create]
  def show
    respond_with Listing.find(params[:id])
  end

  def index
    if not_authorized_to_see_listings(params)
        render json: {}, status: 401
    end
    listings = Listing.search(params).page(params[:page]).per(params[:per_page])
    render json: listings, meta: pagination(listings, params[:per_page])
  end

  def create
    listing = current_user.listings.build(listing_params)
    if listing.save
      render json: listing, status: 201, location: [:api, listing]
    else
      render json: { errors: listing.errors }, status: 422
    end
  end

  def update
    listing = current_user.listings.find(params[:id])
    if listing.update(listing_params)
      render json: listing, status: 200, location: [:api, listing]
    else
      render json: { errors: listing.errors }, status: 422
    end
  end

  def destroy
    listing = current_user.listings.find(params[:id])
    listing.destroy
    head 204
  end

  private
  def not_authorized_to_see_listings(params)
    params["user_ids"].present? && ([current_user.id.to_s] != params["user_ids"])
  end


  def listing_params
    params.require(:data).require(:attributes).permit(:title, :phone, :price, :published, :value, :location)
  end
end
