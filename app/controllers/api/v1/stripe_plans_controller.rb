class Api::V1::StripePlansController < ApplicationController
  respond_to :json

  def show
      stripe_service = StripeService.new
      plan = stripe_service.get_plan(params[:id])
      render json: plan, status: 200
  end

  def index
    stripe_service = StripeService.new
    plans = stripe_service.get_plans
    render json: plans.to_json, status: 200
  end
end
