class ListingSerializer < ActiveModel::Serializer
  attributes :id, :title, :phone, :price, :published, :value, :location
  has_one :user

  def cache_key
    [object, scope]
  end

  def attributes(*args)
    if current_user.hasActiveSubscription? ||
      current_user.purchased_listings.where(listing_id: @object.id).any?
      super
    else
      attributes_to_remove = [:phone]
      super.except(*attributes_to_remove)
    end
  end
end
