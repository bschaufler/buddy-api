class CheckinSerializer < ActiveModel::Serializer
  attributes :id, :location
  has_one :user

  def cache_key
    [object, scope]
  end
end
