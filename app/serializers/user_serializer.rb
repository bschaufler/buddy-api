class UserSerializer < ActiveModel::Serializer
  attributes :id, :email, :created_at, :updated_at, :authentication_token, :stripe_id
end
