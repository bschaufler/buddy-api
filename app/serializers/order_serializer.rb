class OrderSerializer < ActiveModel::Serializer
  attributes :id, :total
  has_many :listings, serializer: OrderListingSerializer
end
