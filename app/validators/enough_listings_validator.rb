class EnoughListingsValidator < ActiveModel::Validator
  def validate(record)
    record.placements.each do |placement|
      listing = placement.listing
      if placement.quantity > listing.quantity
        record.errors["#{listing.title}"] << "Is out of stock, just #{listing.quantity} left"
      end
    end
  end
end