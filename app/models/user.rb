require 'pry'

class User < ActiveRecord::Base
  validates :authentication_token, uniqueness: true
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  before_create :generate_authentication_token, :create_stripe_user

  def generate_authentication_token
    begin
      self.authentication_token = Devise.friendly_token
    end while self.class.exists?(authentication_token: authentication_token)
  end

  def create_stripe_user
      stripe = StripeService.new
      self.stripe_id = stripe.create_customer()
  end

  has_many :listings, dependent: :destroy
  has_many :orders, dependent: :destroy
  has_many :checkins, dependent: :destroy
  has_many :subscriptions, dependent: :destroy
  has_many :purchased_listings, dependent: :destroy

  def hasActiveSubscription?
    self.subscriptions.where(is_active: true).any?
  end
end
