class Placement < ActiveRecord::Base
  belongs_to :order, inverse_of: :placements
  belongs_to :listing, inverse_of: :placements
end
