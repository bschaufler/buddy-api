require 'securerandom'

class StripeUser
  include ActiveModel::Serializers::JSON

  attr_accessor :default_source, :account_balance, :id, :account_balance, :created, :currency, :delinquent, :description,
                :discount, :email, :livemode, :sources

  def attributes
    {'account_balance' => nil}
    {'default_source' => nil}
  end

  def initialize(user)
    @id = SecureRandom.uuid
    @account_balance = user.account_balance
    @created = user.created
    @currency = user.currency
    @delinquent = user.delinquent
    @description = user.description
    @discount = user.discount
    @email = user.email
    @livemode = user.livemode
    @sources = user.sources
  end
end
