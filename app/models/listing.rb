class Listing < ActiveRecord::Base
  validates :title, :user_id, presence: true
  validates :price, numericality: { greater_than_or_equal_to: 0 },
      presence: true

  belongs_to :user
  has_many :placements
  has_many :orders, through: :placements

  scope :filter_by_title, lambda { |keyword|
                          where("lower(title) LIKE ?", "%#{keyword.downcase}%" )
                        }
  scope :filter_by_location, lambda { |location|
                          where("lower(location) LIKE ?", "%#{location.downcase}%" )
                           }

  scope :above_or_equal_to_price, lambda { |price|
                                  where("price >= ?", price)
                                }

  scope :below_or_equal_to_price, lambda { |price|
                                  where("price <= ?", price)
                                }
  scope :recent, -> {
                   order(:updated_at)
               }

  def self.search(params = {})
    listings = params[:user_ids].present? ? Listing.where(user_id: params[:user_ids]) : Listing.all
    listings = params[:listing_ids].present? ? listings.where(id: params[:listing_ids]) : listings
    listings = listings.filter_by_location(params[:location]) if params[:location]
    listings = listings.filter_by_title(params[:keyword]) if params[:keyword]
    listings = listings.above_or_equal_to_price(params[:min_price].to_f) if params[:min_price]
    listings = listings.below_or_equal_to_price(params[:max_price].to_f) if params[:max_price]
    listings = listings.recent(params[:recent]) if params[:recent].present?
    listings
  end
end
