class Order < ActiveRecord::Base
  belongs_to :user
  before_validation :set_total!

  validates :total, presence: true

  validates_with EnoughListingsValidator

  has_many :placements
  has_many :listings, through: :placements

  def set_total!
    self.total = 0
    placements.each do |placement|
      self.total += placement.listing.price * placement.quantity
    end
  end

  def build_placements_with_listing_ids_and_quantities(listing_ids_and_quantities)
    listing_ids_and_quantities.each do |listing_id_and_quantity|
      id, quantity = listing_id_and_quantity # [1,5]

      self.placements.build(listing_id: id, quantity: quantity)
    end
  end
end
