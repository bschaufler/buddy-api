require 'stripe'
require 'pry'

class StripeService

  def initialize
    Stripe.api_key = "sk_test_WhRMRNx3J8vHt6LSJVN5nd7j"
  end

  #Charging
  def create_charge(customer_id, amount)
    Stripe::Charge.create(
        :amount => amount,
        :currency => "usd",
        :customer => customer_id,
        :description => "Charge for one time listing"
    )
  end

  def charge_customer(customer_id, card_id, amount, user_email)
    Stripe::Charge.create(
        :amount => amount,
        :currency => "usd",
        :customer => customer_id,
        :source => card_id,
        :description => "user_email"
    )
  end

  #Payment Sources
  def update_card(id, card_id, name)
    cu = Stripe::Customer.retrieve(id)
    card = cu.sources.retrieve(card_id)
    card.name = name
    card.save
  end

  def delete_card(id, card_id)
    cus= Stripe::Customer.retrieve(id)
    cus.sources.retrieve(card_id).delete
  end

  #Customers
  def create_customer(source = '')
    customer_obj = nil
    if !source.empty?
      customer_obj = Stripe::Customer.create(
          :description => ""
      )
    else
      customer_obj = Stripe::Customer.create(
          :description => ""
      )
    end
    customer_obj.id
  end

  def update_customer(id, source)
    cu = Stripe::Customer.retrieve(id)
    cu.sources.create(:source => source)
    cu.save
  end

  def update_customer_default(id, default_source)
    cu = Stripe::Customer.retrieve(id)
    cu.default_source = default_source["id"]
    cu.save
  end

  def get_customer(id)
    cu = Stripe::Customer.retrieve(id)
  end

  def delete_customer(id = '')
    cu = Stripe::Customer.retrieve(id)
    cu.delete
  end

  #Subscrptions
  def create_subscription(customer_id, plan_id, source_id = nil)
    #if source id is present then it will set the default source of customer to this source
    #first before creating subscription so it uses the desired source. per stripe docs.
    Stripe::Subscription.create(
        :customer => customer_id,
        :plan => plan_id,
        :source => source_id
    )
  end

  def get_subscription(subscription_id)
    Stripe::Subscription.retrieve(subscription_id)
  end

  def update_subscription(subscription_id, plan_id)
    subscription = Stripe::Subscription.retrieve(subscription_id)
    subscription.plan = plan_id
    subscription.save
  end

  def cancel_subscription(subscription_id)
    sub = Stripe::Subscription.retrieve(subscription_id)
    sub.delete
  end

  def check_customer(customer_id, subscription_id)
    customer = get_customer(customer_id)
    if (customer["delinquent"] == false)
      return true;
    end
  end

  def check_subscription(subscriptions_id)
    sub = get_subscription(subscriptions_id)
    plan_id = sub["plan"]["id"]
    if (sub["status"] != "active")
      return { :is_active => false, :plan_id => plan_id }
    else
      return { :is_active => true, :plan_id => plan_id }
    end
  end
  #plans
  def get_plans
    Stripe::Plan.list(:limit => 3)
  end

  def get_plan(id)
    Stripe::Plan.retrieve(id)
  end

  #helper methods

  def createFakeCard(customer)
    customer = Stripe::Customer.retrieve(customer)
    customer.sources.create(:source => {
                              :object =>  'card',
                              :exp_month => '11',
                              :exp_year => '18',
                              :number => '4242424242424242'
                            })
  end
end
