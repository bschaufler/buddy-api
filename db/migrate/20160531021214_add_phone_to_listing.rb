class AddPhoneToListing < ActiveRecord::Migration
  def change
    add_column :listings, :phone, :string
    add_index :listings, :phone
  end
end
