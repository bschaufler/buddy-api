class CreateSubscriptions < ActiveRecord::Migration
  def change
    create_table :subscriptions do |t|
      t.string :stripe_subscription_id
      t.integer :user_id

      t.timestamps null: false
    end
    add_index :subscriptions, :user_id
  end
end
