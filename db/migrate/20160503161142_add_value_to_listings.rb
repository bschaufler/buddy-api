class AddValueToListings < ActiveRecord::Migration
  def change
    add_column :listings, :value, :string
  end
end
