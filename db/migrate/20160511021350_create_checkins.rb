class CreateCheckins < ActiveRecord::Migration
  def change
    create_table :checkins do |t|
      t.string :location
      t.integer :user_id

      t.timestamps null: false
    end
    add_index :checkins, :user_id
  end
end
