class AddQuantityToListings < ActiveRecord::Migration
  def change
    add_column :listings, :quantity, :integer, default: 0
  end
end
