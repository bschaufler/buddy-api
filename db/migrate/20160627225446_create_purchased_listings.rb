class CreatePurchasedListings < ActiveRecord::Migration
  def change
    create_table :purchased_listings do |t|
      t.integer :listing_id
      t.integer :user_id

      t.timestamps null: false
    end
    add_index :purchased_listings, :user_id
  end
end
