require 'api_constraints'

Rails.application.routes.draw do
  devise_for :users,
             controllers: {
              sessions: 'sessions',
              registrations: 'users/registrations',
              passwords: 'users/passwords'
  }
  # Api definition
  # Api definition
  namespace :api, defaults: { format: :json }, path: '/'  do
    scope module: :v1, constraints: ApiConstraints.new(version: 1, default: true) do
          # We are going to list our resources here
          resources :users, :only => [:show, :create, :update, :destroy] do
            resources :listings, :only => [:show, :index, :create, :update, :destroy]
            resources :orders, :only => [:index, :show, :create]
          end
          resources :sessions, :only => [:create, :destroy]
          resources :checkins, :only => [:show, :index, :create, :update, :destroy]
          resources :listings, :only => [:show, :index, :create, :update, :destroy]
          resources :stripe_users, :only => [:show, :index, :update, :destroy], path: '/stripe-users'
          resources :stripe_subscriptions, :only => [:create, :show, :index, :update, :destroy], path: '/stripe-subscriptions'
          resources :stripe_plans, :only => [:show, :index, :update, :destroy], path: '/stripe-plans'
          resources :one_time_purchase, :only => [:create], path: '/one-time-purchase'
          end
end
end
