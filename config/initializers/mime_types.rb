# Be sure to restart your server when you modify this file.

# Add new mime types for use in respond_to blocks:
# Mime::Type.register "text/richtext", :rtf
# Be sure to restart your server when you modify this file.

Mime::Type.register 'application/vnd.api+json', :json_api, [:json]

# Add to default parser so that json api is parsed in the same way as json
ActionDispatch::ParamsParser::DEFAULT_PARSERS[Mime::JSON_API] = :json

module ActionController
  module Renderers
    # here we are duplicating the behavior of
    # ActionController::Renderers.add
    # so as to make our json_api renderer
    # this is safer than calling add with our
    # own block because we would be copying
    # part of rails' own json renderer, poorly
    RENDERERS << :json_api
    alias_method :_render_with_renderer_json_api, :_render_with_renderer_json
  end
end

module ActionController
  module Serialization
    # ActiveModel::Serializers redefines
    # _render_with_renderer_json, so
    # we must re-alias ours if serialization is used
    alias_method :_render_with_renderer_json_api, :_render_with_renderer_json
  end
end
