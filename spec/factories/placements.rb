FactoryGirl.define do
  factory :placement do
    order
    listing
    quantity 1
  end
end