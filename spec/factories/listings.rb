FactoryGirl.define do
  factory :listing do
    title { FFaker::Product.product_name }
    price { rand() * 100 }
    published false
    user
    quantity 5
  end
end
