require "rails_helper"

describe User do

  before {
    @user = FactoryGirl.build(:user)
  }
  subject { @user }


  it { should respond_to(:email) }
  it { should respond_to(:stripe_id) }
  it { should respond_to(:password) }
  it { should respond_to(:password_confirmation) }
  it { should respond_to(:authentication_token) }
  it { should validate_presence_of(:email) }

  it { should be_valid }

  # it { should validate_uniqueness_of(:email) }
  it { should validate_confirmation_of(:password) }
  it { should allow_value('example@domain.com').for(:email) }
  it { should validate_uniqueness_of(:authentication_token)}

  it { should have_many(:orders) }
  it { should have_many(:listings) }

  describe "#generate_authentication_token!" do
    it "generates a unique token" do
      Devise.stub(:friendly_token).and_return("auniquetoken123")
      @user.generate_authentication_token
      expect(@user.authentication_token).to eql "auniquetoken123"
    end

    it "generates another token when one already has been taken" do
      existing_user = FactoryGirl.create(:user, authentication_token: "auniquetoken123")
      @user.generate_authentication_token
      expect(@user.authentication_token).not_to eql existing_user.authentication_token
    end
  end
  describe "#listings association" do

    before do
      @user.save
      3.times { FactoryGirl.create :listing, user: @user }
    end

    it "destroys the associated listings on self destruct" do
      listing = @user.listings
      @user.destroy
      listing.each do |listing|
        expect(Listing.find(listing)).to raise_error ActiveRecord::RecordNotFound
      end
    end
  end

  # describe "#presence of stripe id" do
  #
  #   before do
  #     @user.save
  #   end
  #
  #   it "has stripe id set" do
  #       expect(@user.stripe_id). to eql "stripe"
  #   end
  # end
end