require 'rails_helper'

describe Listing do
  let(:listing) { FactoryGirl.build :listing }
  subject { listing }
  it { should have_many(:placements) }
  it { should have_many(:orders).through(:placements) }

  it { should respond_to(:title) }
  it { should respond_to(:price) }
  it { should respond_to(:published) }
  it { should respond_to(:user_id) }

  # it { should not_be_published }

  it { should validate_presence_of :title }
  it { should validate_presence_of :price }
  it { should validate_numericality_of(:price).is_greater_than_or_equal_to(0) }
  it { should validate_presence_of :user_id }


  it { should belong_to :user }
  describe ".filter_by_title" do
    before(:each) do
      @listing1 = FactoryGirl.create :listing, title: "A plasma TV"
      @listing2 = FactoryGirl.create :listing, title: "Fastest Laptop"
      @listing3 = FactoryGirl.create :listing, title: "CD player"
      @listing4 = FactoryGirl.create :listing, title: "LCD TV"

    end
    context "when a 'TV' title pattern is sent" do
      it "returns the 2 products matching" do
        expect(Listing.filter_by_title("TV").length).to eql 2
      end

      it "returns the products matching" do
        expect(Listing.filter_by_title("TV").sort).to match_array([@listing1, @listing4])
      end
      end
  end

  describe ".above_or_equal_to_price" do
    before(:each) do
      @listing1 = FactoryGirl.create :listing, price: 100
      @listing2 = FactoryGirl.create :listing, price: 50
      @listing3 = FactoryGirl.create :listing, price: 150
      @listing4 = FactoryGirl.create :listing, price: 99
    end

    it "returns the products which are above or equal to the price" do
      expect(Listing.above_or_equal_to_price(100).sort).to match_array([@listing1, @listing3])
    end
  end

  describe ".below_or_equal_to_price" do
    before(:each) do
      @listing1 = FactoryGirl.create :listing, price: 100
      @listing2 = FactoryGirl.create :listing, price: 50
      @listing3 = FactoryGirl.create :listing, price: 150
      @listing4 = FactoryGirl.create :listing, price: 99
    end

    it "returns the products which are above or equal to the price" do
      expect(Listing.below_or_equal_to_price(99).sort).to match_array([@listing2, @listing4])
    end
  end
  describe ".recent" do
    before(:each) do
      @listing1 = FactoryGirl.create :listing, price: 100
      @listing2 = FactoryGirl.create :listing, price: 50
      @listing3 = FactoryGirl.create :listing, price: 150
      @listing4 = FactoryGirl.create :listing, price: 99

      #we will touch some products to update them
      @listing2.touch
      @listing3.touch
    end

    it "returns the most updated records" do
      expect(Listing.recent).to match_array([@listing3, @listing2, @listing4, @listing1])
    end
  end

  describe ".search" do
    before(:each) do
      @listing1 = FactoryGirl.create :listing, price: 100, title: "Plasma tv"
      @listing2 = FactoryGirl.create :listing, price: 50, title: "Videogame console"
      @listing3 = FactoryGirl.create :listing, price: 150, title: "MP3"
      @listing4 = FactoryGirl.create :listing, price: 99, title: "Laptop"
    end

    context "when title 'videogame' and '100' a min price are set" do
      it "returns an empty array" do
        search_hash = { keyword: "videogame", min_price: 100 }
        expect(Listing.search(search_hash)).to be_empty
      end
    end

    context "when title 'tv', '150' as max price, and '50' as min price are set" do
      it "returns the listing1" do
        search_hash = { keyword: "tv", min_price: 50, max_price: 150 }
        expect(Listing.search(search_hash)).to match_array([@listing1])
      end
    end

    context "when an empty hash is sent" do
      it "returns all the products" do
        expect(Listing.search({})).to match_array([@listing1, @listing2, @listing3, @listing4])
      end
    end

    context "when listing_ids is present" do
      it "returns the product from the ids" do
        search_hash = { listing_ids: [@listing1.id, @listing2.id]}
        expect(Listing.search(search_hash)).to match_array([@listing1, @listing2])
      end
    end
  end
  end