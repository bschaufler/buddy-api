require 'rails_helper'

RSpec.describe Checkin, type: :model do
require 'spec_helper'

describe Checkin do
  let(:checkin) { FactoryGirl.build :checkin }
  subject { checkin }

  it { should respond_to(:location) }
  it { should respond_to(:user_id) }

  end
end
