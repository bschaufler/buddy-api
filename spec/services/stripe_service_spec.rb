require 'pry'

describe StripeService do

  test_source = "tok_17yt8IFB0h93w9CZhzUqdKR6"
  test_card = '4242424242424242'

  def delete_customer
    @stripe_service.delete_customer(@user_id)
  end

  def create_token
    @token = Stripe::Token.create(
        :card => {
            :number => '4242424242424242',
            :exp_month => 4,
            :exp_year => 2017,
            :cvc => "314"
        }
    ).id
  end

  before(:each) do
    @stripe_service = StripeService.new
    @user_id = @stripe_service.create_customer()
  end

  describe "Customer Creation" do
    it "customer is created and has id" do
      expect(@user_id[0..2]).to eq('cus')
      delete_customer
    end
  end

  describe "Customer Deletion" do
    it "customer is deleted" do
      response = delete_customer
      expect(response.deleted).to eq(true)
    end
  end

  describe "Add source to customer" do
    it "add source" do
      cu = Stripe::Customer.retrieve(@user_id)
      cu.description = "Customer for test@example.com"
      cu.source = create_token
      cu.save

      cu = Stripe::Customer.retrieve(@user_id)
      expect(cu.sources.data.first.last4).to eq(test_card.last(4))
      delete_customer
    end
  end

end