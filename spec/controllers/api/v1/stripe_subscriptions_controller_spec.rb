require 'rails_helper'

RSpec.describe Api::V1::StripeSubscriptionsController, type: :controller do
describe "GET #create" do
  before(:each) do
    current_user = FactoryGirl.create :user
    api_authorization_header current_user.authentication_token
    @stripe_service = StripeService.new
    @stripe_service.createFakeCard(current_user.stripe_id)
    get :create, { id: 'plan_buyer'}
  end

  it { should respond_with 201 }
  end
end
