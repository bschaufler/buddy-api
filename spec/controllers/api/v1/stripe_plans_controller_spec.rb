require 'rails_helper'

RSpec.describe Api::V1::StripePlansController, type: :controller do
describe "GET #index" do
    before(:each) do
      current_user = FactoryGirl.create :user
      api_authorization_header current_user.authentication_token
      4.times { FactoryGirl.create :order, user: current_user }
      get :index
    end

    it "returns 2 plans" do
      expect(json_response[:data].length).to eql 2
    end

    it { should respond_with 200 }
end

describe "GET #show" do
  before(:each) do
    current_user = FactoryGirl.create :user
    api_authorization_header current_user.authentication_token
    4.times { FactoryGirl.create :order, user: current_user }
    get :show, { id: 'plan_buyer'}
  end

  it "returns correct plan" do
    expect(json_response[:name]).to eql 'Buyer Plan'
  end

  it { should respond_with 200 }
end
end
