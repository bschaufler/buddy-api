require 'rails_helper'
require 'pry'

RSpec.describe Api::V1::OrdersController, type: :controller do
describe "GET #index" do
  before(:each) do
    current_user = FactoryGirl.create :user
    api_authorization_header current_user.authentication_token
    4.times { FactoryGirl.create :order, user: current_user }
    get :index, user_id: current_user.id
  end

  it "returns 4 order records from the user" do
    orders_response = json_response[:data]
    expect(orders_response.length).to eql 4
  end

  it_behaves_like "paginated list"
  it { should respond_with 200 }
end
describe "GET #show" do
  before(:each) do
    current_user = FactoryGirl.create :user
    api_authorization_header current_user.authentication_token

    @listing= FactoryGirl.create :listing
    @order = FactoryGirl.create :order, user: current_user, listing_ids: [@listing.id]
    get :show, user_id: current_user.id, id: @order.id
  end

  it "returns the user order record matching the id" do
    order_response = json_response[:data]
    expect(order_response[:id].to_i).to eql @order.id
  end

  it "includes the total for the order" do
    order_response = json_response[:data]
    expect(order_response[:attributes][:total]).to eql @order.total.to_s
  end

  it "includes the products on the order" do
    order_response = json_response[:data]
    expect(order_response[:relationships][:listings][:data].length).to eql (1)
  end


  it { should respond_with 200 }
end

describe "POST #create" do
  before(:each) do
    current_user = FactoryGirl.create :user
    api_authorization_header current_user.authentication_token

    listing_1 = FactoryGirl.create :listing
    listing_2 = FactoryGirl.create :listing
    order_params = { listing_ids_and_quantities: [[listing_1.id, 2],[ listing_2.id, 3]] }
    post :create, user_id: current_user.id, order: order_params
  end

  it "returns just user order record" do
    order_response = json_response[:data]
    expect(order_response[:id]).to be_present
  end

  it "embeds the two listing objects related to the order" do
    order_response = json_response[:data]
    expect(order_response[:relationships][:listings][:data].size).to eql 2
  end

  it { should respond_with 201 }
end
end
