require 'rails_helper'
require 'pry'

RSpec.describe Api::V1::ListingsController, type: :controller do
  describe "GET #show" do
    before(:each) do
      current_user = FactoryGirl.create :user
      api_authorization_header current_user.authentication_token
      4.times { FactoryGirl.create :listing }
    end

    context "when is not receiving any listing_ids parameter" do
      before(:each) do
        get :index
      end

      it "returns 4 records from the database" do
        listing_response = json_response[:data]
        expect(listing_response.length).to eql 4
      end

      it "returns the user object into each product" do
        listing_response = json_response[:data]
        listing_response.each do |listing_response|
          expect(listing_response[:relationships][:user][:data][:id]).to be_present
        end
      end

      it_behaves_like "paginated list"

      it { should respond_with 200 }
    end

    context "when listing_ids parameter is sent" do
      before(:each) do
        @user = FactoryGirl.create :user
        3.times { FactoryGirl.create :listing, user: @user }
        get :index, listing_ids: @user.listing_ids
      end

      it "returns just the products that belong to the user" do
        listing_response = json_response[:data]
        listing_response.each do |listing_response|
          expect(listing_response[:relationships][:user][:data][:id].to_i).to eql @user.id
        end
      end
    end
    end

  describe "GET #index" do
    before(:each) do
      current_user = FactoryGirl.create :user
      api_authorization_header current_user.authentication_token
      4.times { FactoryGirl.create :listing }
      get :index
    end

    it "returns 4 records from the database" do
      listing_response = json_response[:data]
      expect(listing_response.length).to eql 4
    end

    it "returns the user object into each product" do
      listing_response = json_response[:data]
      listing_response.each do |listing_response|
        expect(listing_response[:relationships][:user]).to be_present
      end
    end

    it { should respond_with 200 }
  end

describe "POST #create" do
  context "when is successfully created" do
    before(:each) do
      user = FactoryGirl.create :user
      api_authorization_header user.authentication_token
      @listing_attributes = FactoryGirl.attributes_for :listing
      @listing_payload = {"data"=>
           {"attributes"=>
                @listing_attributes,
            "type"=>"listings"},
       "format"=>:json,
       "controller"=>"api/v1/listings",
       "action"=>"create",
       "id"=>"1"}
      post :create, @listing_payload
    end

    it "renders the json representation for the listing record just created" do
      listing_response = json_response[:data]
      expect(listing_response[:attributes][:title]).to eql @listing_attributes[:title]
    end

    it { should respond_with 201 }
  end

  context "when is not created" do
    before(:each) do
      user = FactoryGirl.create :user
      @invalid_listing_attributes = { title: "Smart TV", price: "Twelve dollars" }
      api_authorization_header user.authentication_token
      post :create, {"data"=>
                         {"id"=>"1",
                          "attributes"=>
                              {"title"=>"An expensive TV",
                               "published"=>false,
                               "price"=>"",
                               "location"=>"A-Basin",
                               "value"=>"101"},
                          "type"=>"listings"},
                     "format"=>:json,
                     "controller"=>"api/v1/listings",
                     "action"=>"update",
                     "id"=>"1"}
    end

    it "renders an errors json" do
      listing_response = json_response
      expect(listing_response).to have_key(:errors)
    end

    it "renders the json errors on whye the user could not be created" do
      listing_response = json_response
      expect(listing_response[:errors][:price]).to include "is not a number"
    end

    it { should respond_with 422 }
  end
end
describe "PUT/PATCH #update" do
  before(:each) do
    @user = FactoryGirl.create :user
    @listing = FactoryGirl.create :listing, user: @user
    api_authorization_header @user.authentication_token
  end

  context "when is successfully updated" do
    before(:each) do
      patch :update, {"data"=>
                          {"id"=>"1",
                           "attributes"=>
                               {"title"=>"An expensive TV",
                                "published"=>false,
                                "price"=>50,
                                "location"=>"A-Basin",
                                "value"=>"101"},
                           "type"=>"listings"},
                      "format"=>:json,
                      "controller"=>"api/v1/listings",
                      "action"=>"update",
                      "id"=>"1"}
    end

    it "renders the json representation for the updated user" do
      listing_response = json_response[:data]
      expect(listing_response[:attributes][:title]).to eql "An expensive TV"
    end

    it { should respond_with 200 }
  end

  context "when is not updated" do
    before(:each) do
      patch :update, {"data"=>
                          {"id"=>"1",
                           "attributes"=>
                               {"title"=>"An expensive TV",
                                "published"=>false,
                                "price"=>-1,
                                "location"=>"A-Basin",
                                "value"=>"101"},
                           "type"=>"listings"},
                      "format"=>:json,
                      "controller"=>"api/v1/listings",
                      "action"=>"update",
                      "id"=>"1"}
    end

    it "renders an errors json" do
      listing_response = json_response
      expect(listing_response).to have_key(:errors)
    end

    it "renders the json errors on whye the user could not be created" do
      listing_response = json_response
      expect(listing_response[:errors][:price][0]).to include "must be greater than or equal to 0"
    end

    it { should respond_with 422 }
  end
end

describe "DELETE #destroy" do
  before(:each) do
    @user = FactoryGirl.create :user
    @listing = FactoryGirl.create :listing, user: @user
    api_authorization_header @user.authentication_token
    delete :destroy, { user_id: @user.id, id: @listing.id }
  end

  it { should respond_with 204 }
  end
end

