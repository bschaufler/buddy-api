require 'rails_helper'
require 'pry'

RSpec.describe Api::V1::StripeUsersController, type: :controller do
describe "GET #show" do
  before(:each) do
    @user = FactoryGirl.create :user
    api_authorization_header @user.authentication_token
    get :show, id: @user.id
  end

  # it "returns stripe user" do
  #   stripe_user = json_response
  #   expect(@user.stripe_id).to eql stripe_user[:id]
  # end

end
describe "GET #show unauthorized" do
  before(:each) do
    @user = FactoryGirl.create :user
    api_authorization_header @user.authentication_token
    get :show, id: 45
  end

    it { should respond_with 403 }

end
end
